<?php

/*
The following four classes will be required for the assignment.
Vehicle: It will represent what type of vehicle is it ex. small, medium, large.
Customer: It will represents customer name and what type of customer it owns.
			A customer can have more than on vehicles.
Parking: It will represent the parking size and parking spot number.
Ticket: It will represent the ticket number and which vehicle is placed at which parking spot.

*/

Interface ParkingSpots
{
	public function addParkingSpot();

	public function findParkingSpots(Vehicle $vehicle);
}

Interface TicketInterface
{
	public function redeemVehicle();
}

Interface VehicleInterface
{
	public function bookTicket(Parking $parking, $ticket_number);
}

/**
* 
*/
class Customer
{
	private $name;
	private $vehicles = [];

	function __construct(Vehicle $vehicle, $name)
	{
		array_push($this->vehicles, $vehicle);
		$this->name = $name;
	}

	public function addVehicle(Vehicle $vehicle)
	{
		array_push($this->vehicles, $vehicle);
	}

	public function getCustomer()
	{
		echo "My Customer<br>";
		echo $this->name."<br>";
		foreach ($this->vehicles as $key => $value) {
			echo ($key+1).":";
			$value->getVehicle();
			echo "<br>";
		}
	}
}

/**
* 
*/
class Parking implements ParkingSpots
{
	private $size;
	private $spot_number;

	function __construct($size, $spot_number)
	{
		$this->size = $size;
		$this->spot_number = $spot_number;
	}

	public function addParkingSpot()
	{
		// code to add parking spot
	}

	public function findParkingSpots(Vehicle $vehicle)
	{
		// code to find parking spot for vehicle
	}

	public function getParking()
	{
		echo "My Parking<br>";
		echo $this->size."<br>";
		echo $this->spot_number."<br>";
	}
}

/**
* 
*/
class Ticket implements TicketInterface
{
	private $ticket_number;
	private $vehicle;
	private $parking;

	function __construct(Vehicle $vehicle, Parking $parking, $ticket_number)
	{
		$this->vehicle = $vehicle;
		$this->parking = $parking;
		$this->ticket_number = $ticket_number;
	}

	public function redeemVehicle()
	{
		// code to remove ticket and free parking
	}

	public function getTicket()
	{
		echo "My Ticket<br>";
		echo $this->ticket_number."<br>";
		$this->vehicle->getVehicle();
		$this->parking->getParking();
	}
}

/**
* 
*/
class Vehicle implements VehicleInterface
{
	private $type;
	private $size;
	private $vehicle_number;

	function __construct($type, $size, $vehicle_number)
	{
		$this->type = $type;
		$this->size = $size;
		$this->vehicle_number = $vehicle_number;
	}

	public function getVehicle()
	{
		echo "My Vehicle<br>";
		echo $this->type."<br>";
		echo $this->size."<br>";
		echo $this->vehicle_number."<br>";
	}

	public function bookTicket(Parking $parking, $ticket_number)
	{
		$ticket = new Ticket($this, $parking, $ticket_number);
		return $ticket;
	}
}

$vehicle = new Vehicle("Car", "Large", "1234");
$customer = new Customer($vehicle, "Sam");

$customer->getCustomer();

$vehicle1 = new Vehicle("Car", "Medium", "1111");
$customer->addVehicle($vehicle1);

$customer->getCustomer();

$parking1 = new Parking("Large", "A1");
$parking2 = new Parking("Large", "B1");
$parking3 = new Parking("Medium", "C1");
$parking4 = new Parking("Small", "C1");

$ticket = $vehicle1->bookTicket($parking1, 123123);
$ticket->getTicket();

?>